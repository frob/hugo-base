include .env.project

PROJECT_NAME?=hugo-base

DOCKER_RUN=docker run --rm -it

init:
	$(DOCKER_RUN) \
  -v $(PWD):/src \
  klakegg/hugo:0.107.0-ext-alpine new site $(PROJECT_NAME)

run:
	$(DOCKER_RUN) \
  -v $(PWD)/$(PROJECT_NAME):/src \
  klakegg/hugo:0.107.0-ext-alpine serve

build:
	$(DOCKER_RUN) \
  -v $(PWD)/$(PROJECT_NAME):/src \
  klakegg/hugo:0.107.0-ext-alpine