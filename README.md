# hugo-base

This is intended to be a starting place for hugo based projects. It provides a Makefile with common targets for initialization and building in docker to limit the installed requirements.

## Getting started

Add a `.env.project` file and configure the values that will be used by the make file. This project is built as a fork and forget project.

### Environment variables

- `PROJECT_NAME` - This is the name that will be passed to the `hogo new site` command. Ultimately, this will be used to dictate the directory that will contain the hugo site.


